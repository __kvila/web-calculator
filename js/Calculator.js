function calculateNumbers(){
  var numberOne = document.getElementById("firstnumber").value,
      numberTwo = document.getElementById("secondnumber").value,
      operation = document.getElementById("operation").value,
      result;

  if(operation === "+"){
    result = parseFloat(numberOne) + parseInt(numberTwo);
  } else 
      if(operation === "-"){
        result = parseFloat(numberOne) - parseInt(numberTwo);
      } else 
          if(operation === "*"){
            result = parseFloat(numberOne) * parseInt(numberTwo);
          } else 
            if(operation === "/"){
              result = parseFloat(numberOne) / parseInt(numberTwo);
            }

  if(result === undefined){
    alert("Error! You did not fill in all the fields or entered an incorrect operation. Fix it and try again.")
  }

  document.getElementById("result").value = result;

}
